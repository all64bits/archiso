# Configs for creation of a custom live CD ISO image

## Usage

**Install archiso**

	$ sudo pacman -S archiso

**Run mkarchiso**

**Note:** Assumes that the archiso repo is cloned to the current directory.

	$ sudo mkarchiso -v -w /tmp -o . ./archiso

This command will output the completed .iso file to the current directory.

## Differences from standard ISO

* clonezilla & tmux preinstalled
* sshd service already enabled

## References

[Arch Wiki page for Archiso](https://wiki.archlinux.org/index.php/Archiso)
